package com.weather.viewmodels

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.weather.data.repositories.MainRepository
import com.weather.data.response.GetCityResponseDTO
import com.weather.data.response.GetConditionsResponseDTO
import com.weather.data.response.GetForecastResponseDTO
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

/**
 * Created by JOSER on 10,February,2020.
 */

@Singleton
class MainViewModel @Inject constructor() : ViewModel() {

    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)

    @Inject
    lateinit var mainRepository: MainRepository

    val getCityDataResp = MutableLiveData<GetCityResponseDTO>()


    fun getCityData(location: Location) {
        scope.launch {
            getCityDataResp.postValue(
                mainRepository.getCityData(
                    location.latitude.toString(),
                    location.longitude.toString()
                )
            )
        }
    }

    val getConditionsResp=MutableLiveData<GetConditionsResponseDTO>()

    fun getConditionsData(city:String){
        scope.launch {
            getConditionsResp.postValue(
                mainRepository.getConditionsData(city)
            )
        }
    }

    val getForecastResp=MutableLiveData<GetForecastResponseDTO>()

    fun getForecastData(city:String){
        scope.launch {
            getForecastResp.postValue(
                mainRepository.getForecastData(city)
            )
        }
    }
}