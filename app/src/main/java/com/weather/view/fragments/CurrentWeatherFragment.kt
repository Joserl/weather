package com.weather.view.fragments

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.weather.R
import com.weather.Weather
import com.weather.data.response.GetCityResponseDTO
import com.weather.data.response.GetConditionsResponseDTO
import com.weather.data.response.GetForecastResponseDTO
import com.weather.databinding.FragmentCurrentWeatherBinding
import com.weather.utils.Util
import com.weather.view.activities.MainActivity
import com.weather.view.adapters.ForecastAdapter
import com.weather.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_current_weather.*
import kotlinx.android.synthetic.main.view_current_weather_skeleton.*
import kotlinx.android.synthetic.main.view_forecast_list_skeleton.*
import javax.inject.Inject
import kotlin.math.log

/**
 * Created by JOSER on 10,February,2020.
 */
class CurrentWeatherFragment : Fragment() {

    @Inject
    lateinit var mainViewModel: MainViewModel
    private val LOCATION_PERMISSION_VALUE = 1
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var currentLoc: Location? = null
    lateinit var binding: FragmentCurrentWeatherBinding
    private lateinit var alertDialog: AlertDialog.Builder
    private lateinit var connectivityManager: ConnectivityManager
    private lateinit var networkRequest: NetworkRequest

    @Inject
    lateinit var util: Util

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        (activity?.application as Weather).appComponent.inject(this)
        connectivityManager =
            context?.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()

        //return inflater.inflate(R.layout.fragment_current_weather, container, false)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_current_weather, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        alertDialog = AlertDialog.Builder(context!!)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        requestPermissionLocation()

        tvWithoutConn.visibility = if (util.checkNetworkConnection()) {
            View.GONE
        } else {
            View.VISIBLE
        }

        startLoader()
        mainViewModel.getCityDataResp.observe(viewLifecycleOwner, getCityDataListener)
        mainViewModel.getConditionsResp.observe(viewLifecycleOwner, getConditionsListener)
        mainViewModel.getForecastResp.observe(viewLifecycleOwner, getForecastListener)

    }


    private val getCityDataListener = Observer<GetCityResponseDTO> {
        when (it.code) {
            "0" -> {
                mainViewModel.getConditionsData(it.city.key)
                mainViewModel.getForecastData(it.city.key)
                binding.city = it.city
            }
            else -> {
                Snackbar.make(view!!, it.message.toString(), Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private val getConditionsListener = Observer<GetConditionsResponseDTO> {
        when (it.code) {
            "0" -> {
                Log.i("CONDITIONS", it.condition.toString())
                binding.condition = it.condition
            }
            else -> {
                Log.e("CONDITIONS", it.message.toString())
                Snackbar.make(view!!, it.message.toString(), Snackbar.LENGTH_LONG).show()
            }
        }

    }

    private val getForecastListener = Observer<GetForecastResponseDTO> {
        when (it.code) {
            "0" -> {
                Log.i("FORECAST", it.forecastList.toString())
                forecastList.apply {
                    adapter = ForecastAdapter(it.forecastList)
                    layoutManager = LinearLayoutManager(context)
                }

            }
            else -> {
                Log.e("FORECAST", it.message.toString())
                Snackbar.make(view!!, it.message.toString(), Snackbar.LENGTH_LONG).show()
            }
        }
        hideLoader()
    }

    private fun startLoader() {
        currentWeatherSection.visibility = View.INVISIBLE
        forecastList.visibility = View.INVISIBLE
        shimmer_forecast_list.visibility = View.VISIBLE
        shimmer_forecast_list.startShimmer()
        shimmer_current_weather.visibility = View.VISIBLE
        shimmer_current_weather.startShimmer()
    }

    private fun hideLoader() {
        currentWeatherSection.visibility = View.VISIBLE
        forecastList.visibility = View.VISIBLE
        shimmer_forecast_list.visibility = View.GONE
        shimmer_forecast_list.stopShimmer()
        shimmer_current_weather.visibility = View.GONE
        shimmer_current_weather.stopShimmer()
    }

    private fun requestPermissionLocation() {
        when {
            ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                getFusedLocation()
            }
            else ->
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    LOCATION_PERMISSION_VALUE
                )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        when (requestCode) {
            LOCATION_PERMISSION_VALUE -> {
                if ((ContextCompat.checkSelfPermission(
                        context!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED)
                ) {
                    getFusedLocation()

                } else {
                    if (ContextCompat.checkSelfPermission(
                            context!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) == PackageManager.PERMISSION_DENIED
                    ) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                activity as MainActivity,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            )
                        ) {
                            showAlertPermission()
                        } else {
                            Log.d(tag, "NEVER ASK IS ACTIVE")
                        }
                    }
                }
                return
            }
        }
    }

    private fun getFusedLocation() {
        fusedLocationClient.lastLocation.addOnSuccessListener {
            currentLoc = it
            Log.i("location", currentLoc.toString())
            if (currentLoc != null) {
                connectivityManager.registerNetworkCallback(
                    networkRequest,
                    networkCallback
                )
                mainViewModel.getCityData(currentLoc!!)
            } else {
                Snackbar.make(
                    view!!,
                    resources.getString(R.string.error_location),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun showAlertPermission() {
        alertDialog.setMessage(resources.getString(R.string.alert_permission_requested))
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton("Aceptar") { dialog, _ ->
            requestPermissionLocation()
            dialog.dismiss()
        }
        alertDialog.setNegativeButton("Rechazar") { dialog, _ ->
            dialog.dismiss()
        }
        alertDialog.show()
    }


    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {

        }
    }

    private var networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onLost(network: Network?) {
            Log.d("networkCallback", "networkcallback called from onLost")
            activity?.runOnUiThread {
                tvWithoutConn.visibility = View.VISIBLE
            }
        }

        override fun onUnavailable() {
            Log.d("networkCallback", "networkcallback called from onUnvailable")
            activity?.runOnUiThread {
                tvWithoutConn.visibility = View.VISIBLE
            }
        }

        override fun onLosing(network: Network?, maxMsToLive: Int) {
            Log.d("networkCallback", "networkcallback called from onLosing")
        }

        override fun onAvailable(network: Network?) {
            Log.d("networkCallback", "NetworkCallback network called from onAvailable ")
            activity?.runOnUiThread {
                startLoader()
                tvWithoutConn.visibility = View.GONE
            }
            mainViewModel.getCityData(currentLoc!!)
        }
    }
}