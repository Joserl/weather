package com.weather.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.weather.R
import com.weather.data.database.entities.ForecastDataModel
import com.weather.data.models.Forecast
import com.weather.databinding.ViewForecastItemBinding

/**
 * Created by JOSER on 11,February,2020.
 */
enum class Actions { ACTION1 }
class ForecastAdapter(
    private val forecastList: List<ForecastDataModel>
) : RecyclerView.Adapter<ForecastAdapter.ForecastHolder>() {

    private lateinit var itemBinding:ViewForecastItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastHolder {
        itemBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.view_forecast_item, parent, false)
        return ForecastHolder(itemBinding)
    }

    override fun getItemCount(): Int=forecastList.size

    override fun onBindViewHolder(holder: ForecastHolder, position: Int)=holder.bind(forecastList[position])

    class ForecastHolder(private val itemBinding: ViewForecastItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(forecast: ForecastDataModel){
            itemBinding.forecast=forecast
        }
    }
}