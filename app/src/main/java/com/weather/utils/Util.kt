package com.weather.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by JOSER on 11,February,2020.
 */
class Util(private val context: Context) {

    fun checkNetworkConnection():Boolean{

        val manager=context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val network=manager.activeNetworkInfo

        return (network!=null && network.isConnected)
    }

    companion object {
        @JvmStatic
        fun formatEpochDate(millis:String):String{

            val date= Date(millis.toLong()*1000)
            val format= SimpleDateFormat("EEEE dd MMM, yyyy", Locale.getDefault())

            return format.format(date)
        }
    }
}