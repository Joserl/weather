package com.weather.data.response

import com.weather.data.database.entities.CityDataModel
import com.weather.data.models.City

/**
 * Created by JOSER on 10,February,2020.
 */
data class GetCityResponseDTO(var city: CityDataModel):ResponseDTO("0",""){
    constructor():this(CityDataModel())
}