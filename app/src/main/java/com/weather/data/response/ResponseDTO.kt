package com.weather.data.response

import com.google.gson.annotations.SerializedName

/**
 * Created by JOSER on 10,February,2020.
 */
open class ResponseDTO(
    @SerializedName("Code")
    var code:String,
    @SerializedName("Message")
    var message:String?)