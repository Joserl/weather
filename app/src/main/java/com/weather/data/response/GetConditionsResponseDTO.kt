package com.weather.data.response

import com.weather.data.database.entities.ConditionDataModel

data class GetConditionsResponseDTO(var condition: ConditionDataModel):ResponseDTO("0",""){
    constructor():this(ConditionDataModel())
}