package com.weather.data.response

import com.google.gson.annotations.SerializedName
import com.weather.data.database.entities.ForecastDataModel
import com.weather.data.models.Forecast

/**
 * Created by JOSER on 11,February,2020.
 */
data class GetForecastResponseDTO(
    @SerializedName("DailyForecasts")
    val forecastList: List<ForecastDataModel>
):ResponseDTO("0","") {
    constructor():this(listOf())
}