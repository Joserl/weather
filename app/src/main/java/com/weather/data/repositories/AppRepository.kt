package com.weather.data.repositories

import com.weather.data.api.WeatherApiService
import com.weather.data.database.AppDataBase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

/**
 * Created by JOSER on 10,February,2020.
 */
@Singleton
open class AppRepository @Inject constructor() {

    /**
     * Punto de partida para acceder a datos remotos
     */
    @Inject
    protected lateinit var remoteStorage: WeatherApiService

    /**
     * Punto de partida para acceder a datos locales del dispositivo
     */
    @Inject
    protected lateinit var localStorage: AppDataBase

}
