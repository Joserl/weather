package com.weather.data.repositories

import com.google.gson.Gson
import com.weather.data.database.AppDataBase
import com.weather.data.database.entities.CityDataModel
import com.weather.data.database.entities.ConditionDataModel
import com.weather.data.database.entities.ForecastDataModel
import com.weather.data.models.*
import com.weather.data.response.GetCityResponseDTO
import com.weather.data.response.GetConditionsResponseDTO
import com.weather.data.response.GetForecastResponseDTO
import com.weather.utils.APY_KEY
import com.weather.utils.Util
import java.lang.StringBuilder
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by JOSER on 10,February,2020.
 */
@Singleton
class MainRepository @Inject constructor() : AppRepository() {

    @Inject
    lateinit var appDataBase: AppDataBase

    @Inject
    lateinit var util: Util

    suspend fun getCityData(latitud: String, longitud: String): GetCityResponseDTO {
        var getCityResponseDTO: GetCityResponseDTO

        try {
            if(util.checkNetworkConnection()) {
                val location = StringBuilder(latitud).append(",").append(longitud).toString()
                val resp = remoteStorage.getCityData(APY_KEY, location).await()

                getCityResponseDTO = if (resp.isSuccessful) {
                    val city = resp.body() ?: City()
                    val cityDataModel=CityDataModel(city.key,city.localizedName)
                    appDataBase.citiDao().save(cityDataModel)
                    GetCityResponseDTO(cityDataModel)
                } else {
                    Gson().fromJson(resp.errorBody()?.string(), GetCityResponseDTO::class.java)
                        .apply {
                            code = if (code.isEmpty()) "100" else code
                        }
                }
            }else{
                val cityList=appDataBase.citiDao().getAll()
                getCityResponseDTO= GetCityResponseDTO(CityDataModel(cityList[0].key,cityList[0].localizedName))
            }

        } catch (e: Exception) {
            getCityResponseDTO = GetCityResponseDTO(CityDataModel("", ""))
            getCityResponseDTO.code = "100"
            getCityResponseDTO.message = e.message
        }

        return getCityResponseDTO
    }



    suspend fun getConditionsData(city:String): GetConditionsResponseDTO {
        var getConditionsResponseDTO:GetConditionsResponseDTO
        try {
            if(util.checkNetworkConnection()) {
                val resp = remoteStorage.getCurrentConditions(city, APY_KEY).await()

                getConditionsResponseDTO = if (resp.isSuccessful) {
                    val conditions = resp.body() ?: listOf()
                    val conditionDataModel=ConditionDataModel(conditions[0].weatherText,conditions[0].temperature.metric.value,city)
                    appDataBase.conditionDao().save(conditionDataModel)
                    GetConditionsResponseDTO(conditionDataModel)
                } else {
                    Gson().fromJson(
                        resp.errorBody()?.string(),
                        GetConditionsResponseDTO::class.java
                    )
                        .apply {
                            code = if (code.isEmpty()) "100" else code
                        }
                }
            }else{
                val condition=appDataBase.conditionDao().getByCity(city)
                getConditionsResponseDTO= GetConditionsResponseDTO(condition)
            }
        } catch (e: Exception) {
            getConditionsResponseDTO = GetConditionsResponseDTO(ConditionDataModel("","",""))
            getConditionsResponseDTO.code = "100"
            getConditionsResponseDTO.message = e.message
        }

        return getConditionsResponseDTO
    }

    suspend fun getForecastData(city:String): GetForecastResponseDTO {
        var getForecastResponseDTO:GetForecastResponseDTO
        try {
            if(util.checkNetworkConnection()) {
                val resp = remoteStorage.getForecast5Days(city, APY_KEY).await()

                getForecastResponseDTO = if (resp.isSuccessful) {
                    val resp = resp.body() ?: ForecastHistory()
                    val forecastList=saveForecast(resp.forecastList, city)
                    GetForecastResponseDTO(forecastList)
                } else {
                    Gson().fromJson(resp.errorBody()?.string(), GetForecastResponseDTO::class.java)
                        .apply {
                            code = if (code.isEmpty()) "100" else code
                        }
                }
            }else{
                val forecastList=appDataBase.forecastDao().getAllByCity(city)
                getForecastResponseDTO=GetForecastResponseDTO(forecastList)
            }
        } catch (e: Exception) {
            getForecastResponseDTO = GetForecastResponseDTO()
            getForecastResponseDTO.code = "100"
            getForecastResponseDTO.message = e.message
        }

        return getForecastResponseDTO
    }


    private fun saveForecast(forecastList:List<Forecast>,city: String):List<ForecastDataModel>{
       val list= arrayListOf<ForecastDataModel>()
        forecastList.forEach {
            val fm=ForecastDataModel(0,it.date,it.temperature.minimun.value,it.temperature.maximun.value,it.day.iconPhrase,city)
            list.add(fm)
            appDataBase.forecastDao().save(fm)
        }
        return list
    }
}