package com.weather.data.api

import com.weather.data.models.City
import com.weather.data.models.Condition
import com.weather.data.models.Forecast
import com.weather.data.models.ForecastHistory
import com.weather.data.response.GetForecastResponseDTO
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface WeatherApiService {


    @GET("/locations/v1/cities/geoposition/search?language=es-mx")
    fun getCityData(@Query("apikey") apiKey:String,@Query("q")latLong:String): Deferred<Response<City>>

    @GET("/currentconditions/v1/{locationKey}?language=es-mx")
    fun getCurrentConditions(@Path("locationKey") cityKey:String,@Query("apikey") apiKey:String):Deferred<Response<List<Condition>>>

    @GET("/forecasts/v1/daily/5day/{locationKey}?language=es-mx&metric=true")
    fun getForecast5Days(@Path("locationKey") cityKey:String,@Query("apikey") apiKey:String):Deferred<Response<ForecastHistory>>

}