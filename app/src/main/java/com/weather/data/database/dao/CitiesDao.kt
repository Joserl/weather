package com.weather.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.weather.data.database.entities.CityDataModel

/**
 * Created by JOSER on 10,February,2020.
 */

@Dao
interface CitiesDao {

    @Insert(onConflict = REPLACE)
    fun save(city: CityDataModel)

    @Query("select * from citydatamodel")
    fun getAll():List<CityDataModel>
}