package com.weather.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.weather.data.database.entities.ConditionDataModel

/**
 * Created by JOSER on 11,February,2020.
 */
@Dao
interface ConditionDao {

    @Insert(onConflict = REPLACE)
    fun save(condition: ConditionDataModel)

    @Query("select * from conditiondatamodel where cityKey=:key")
    fun getByCity(key:String):ConditionDataModel
}