package com.weather.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.weather.data.database.entities.ForecastDataModel

/**
 * Created by JOSER on 11,February,2020.
 */

@Dao
interface ForecastDao {
    @Insert(onConflict = REPLACE)
    fun saveAll(forecastList: List<ForecastDataModel>)

    @Insert(onConflict = REPLACE)
    fun save(forecast:ForecastDataModel)

    @Query("select * from forecastdatamodel where cityKey=:key")
    fun getAllByCity(key:String):List<ForecastDataModel>
}