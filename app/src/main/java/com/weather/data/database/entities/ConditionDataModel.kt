package com.weather.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by JOSER on 10,February,2020.
 */

@Entity
data class ConditionDataModel(
    @ColumnInfo
    val weatherText:String,
    @ColumnInfo
    val temperature: String,
    @PrimaryKey
    val cityKey:String){
    constructor():this("", "","")
}