package com.weather.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by JOSER on 11,February,2020.
 */

@Entity
data class ForecastDataModel(
    @PrimaryKey(autoGenerate = true)
    val id:Int,
    @ColumnInfo
    val date:String,
    @ColumnInfo
    val minTemp: String,
    @ColumnInfo
    val maxTemp: String,
    @ColumnInfo
    val weatherText:String,
    @ColumnInfo
    val cityKey:String
) {
    constructor():this(0,"","","","","")
}