package com.weather.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by JOSER on 10,February,2020.
 */

@Entity
data class CityDataModel(
    @PrimaryKey
    @SerializedName("Key")
    val key:String,
    @ColumnInfo
    @SerializedName("LocalizedName")
    val localizedName:String){
    constructor():this("","")
}