package com.weather.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.weather.data.database.dao.CitiesDao
import com.weather.data.database.dao.ConditionDao
import com.weather.data.database.dao.ForecastDao
import com.weather.data.database.entities.CityDataModel
import com.weather.data.database.entities.ConditionDataModel
import com.weather.data.database.entities.ForecastDataModel

/**
 * Created by JOSER on 10,February,2020.
 */

@Database(entities = [CityDataModel::class,ConditionDataModel::class,ForecastDataModel::class], version = 1,exportSchema = false)
abstract  class AppDataBase: RoomDatabase() {

    abstract fun citiDao():CitiesDao

    abstract  fun conditionDao():ConditionDao

    abstract  fun forecastDao():ForecastDao

}