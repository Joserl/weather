package com.weather.data.models

import com.google.gson.annotations.SerializedName
import com.weather.data.response.ResponseDTO

class ForecastHistory(
@SerializedName("DailyForecasts")
val forecastList: List<Forecast>
): ResponseDTO("0","") {
    constructor():this(listOf())
}