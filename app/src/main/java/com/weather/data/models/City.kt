package com.weather.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by JOSER on 10,February,2020.
 */

data class City(
    @SerializedName("Key")
    val key:String,
    @SerializedName("LocalizedName")
    val localizedName:String){
    constructor():this("","")
}