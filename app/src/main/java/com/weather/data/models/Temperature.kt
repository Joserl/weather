package com.weather.data.models

import com.google.gson.annotations.SerializedName

data class Temperature(
    @SerializedName("Metric")
    val metric:Metric,
    @SerializedName("Minimum")
    val minimun:Metric,
    @SerializedName("Maximum")
    val maximun:Metric){
    constructor():this(Metric("",""),Metric("",""),Metric("",""))
}