package com.weather.data.models

import com.google.gson.annotations.SerializedName

data class Metric(
    @SerializedName("Value")
    val value:String,
    @SerializedName("Unit")
    val unit:String)