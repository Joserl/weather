package com.weather.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by JOSER on 10,February,2020.
 */

data class Condition(
    @SerializedName("WeatherText")
    val weatherText:String,
    @SerializedName("Temperature")
    val temperature: Temperature){
    constructor():this("", Temperature())
}