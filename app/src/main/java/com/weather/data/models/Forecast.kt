package com.weather.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by JOSER on 11,February,2020.
 */

data class Forecast(
    @SerializedName("EpochDate")
    val date:String,
    @SerializedName("Temperature")
    val temperature: Temperature,
    @SerializedName("Day")
    val day:Day
) {
    constructor():this("", Temperature(),Day())
}