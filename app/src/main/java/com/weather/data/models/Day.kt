package com.weather.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by JOSER on 11,February,2020.
 */
data class Day(
    @SerializedName("Icon")
    val icon:String,
    @SerializedName("IconPhrase")
    val iconPhrase:String,
    @SerializedName("HasPrecipitation")
    val hasPrecipitation:Boolean
) {
    constructor():this("","",false)
}