package com.weather

import android.app.Application
import com.weather.di.AppComponent
import com.weather.di.DaggerAppComponent
import com.weather.di.modules.AppModule
import com.weather.di.modules.NetModule

/**
 * Created by JOSER on 10,February,2020.
 */
class Weather:Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent= DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .netModule(NetModule())
            .build()
    }

}