package com.weather.di.modules

import android.content.Context
import androidx.room.Room
import com.weather.data.database.AppDataBase
import com.weather.utils.Util
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext()=context

    @Provides
    @Singleton
    fun provideUtils()=Util(context)

   /* @Provides
    @Singleton
    fun provideLocalStorage()= AppDatabase.getInstance(context)*/

    @Provides
    @Singleton
    fun provideRoomDatabase()= Room.databaseBuilder(
        context,
        AppDataBase::class.java, "localStorage"
    )
        .fallbackToDestructiveMigration()
        .build()

}