package com.weather.di

import com.weather.di.modules.AppModule
import com.weather.di.modules.NetModule
import com.weather.view.fragments.CurrentWeatherFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by JOSER on 10,February,2020.
 */
@Singleton
@Component(modules = [AppModule::class, NetModule::class])
interface AppComponent {

    fun inject(currentWeatherFragment: CurrentWeatherFragment)
}